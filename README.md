Barrel project media
--------------------

Find here logos and other media used by the barrel project for its online or
offline presence.


**Barrel, Barrel DB, and the project logo are trademarks of Benoit Chesneau and Enki Multimedia.**


Logos and other medias containing our trademarks are licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.

## Guidelines

For all permitted uses of our trademarks, you may not:

* alter our logos in any way
* place a logo in such close proximity to other content that it is indistinguishable
* make our logo the most distinctive or prominent feature on your website, printed material or other content
* use our logos in a way that suggests any type of association or partnership with Benoit Chesneau or approval, sponsorship or
endorsement by Benoit Chesneau (unless allowed via a license from us)
* use our logos in a way that is harmful, deceptive, obscene or otherwise objectionable to the average person
* use our logos on websites or other places containing content associated with hate speech or illegal activities
* use our logos to, or in connection with, content that disparages us or sullies our reputation
